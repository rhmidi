/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RHMIXXX_gpio_h_INCLUDED
#define RHMIXXX_gpio_h_INCLUDED

#define GPIO_NUM_INPUTS 4
#define GPIO_NUM_OUTPUTS 4
#define GPIO_MIDI_NOTE_OFFSET 32
#define GPIO_MIDI_NOTE_ALL_OUTPUTS GPIO_MIDI_NOTE_OFFSET + 31

void gpio_init(void);

void gpio_out_on(uint8_t num);
void gpio_out_off(uint8_t num);
void gpio_out_toggle(uint8_t num);

void gpio_task(void);

#endif
