/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#include <LUFA/Drivers/Misc/RingBuffer.h>
#include "eventqueue.h"

static RingBuffer_t event_queue;
static uint8_t event_queue_data[16];

void eventqueue_init(void)
{
  RingBuffer_InitBuffer(&event_queue, event_queue_data, sizeof(event_queue_data));
}

uint8_t eventqueue_pop(uint8_t* key, uint8_t* state)
{
  if (RingBuffer_IsEmpty(&event_queue))
    return 0;

  uint8_t event = RingBuffer_Remove(&event_queue);
  *state = (event & 0x80) ? 1 : 0;
  *key = event & 0x7F;
  return 1;
}

void eventqueue_push(uint8_t key, uint8_t state)
{
  uint8_t event = (state) ? 0x80 : 0;
  event |= (key & 0x7F);
  RingBuffer_Insert(&event_queue, event);
}
