/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RHMIXXX_analog_h_INCLUDED
#define RHMIXXX_analog_h_INCLUDED

#define ANALOG_NUM_INPUTS 8
#define ANALOG_MIDI_NOTE_OFFSET 64
#define ANALOG_MIDI_NOTE_ALL_INPUTS ANALOG_MIDI_NOTE_OFFSET + 31

void analog_init(void);
void analog_enable_channel(uint8_t channel);
void analog_disable_channel(uint8_t channel);
uint8_t analog_get_value(uint8_t channel);
void analog_task(void);

#endif
