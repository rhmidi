/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <stdio.h>

/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/
#include <LUFA/Version.h>
#include <LUFA/Drivers/USB/USB.h>
#include "lufa-descriptor-midi.h"

/** LUFA MIDI Class driver interface configuration and state information. This structure is
 *  passed to all MIDI Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_MIDI_Device_t MIDI_Interface =
	{
		.Config =
			{
				.StreamingInterfaceNumber = INTERFACE_ID_AudioStream,
				.DataINEndpoint           =
					{
						.Address          = MIDI_STREAM_IN_EPADDR,
						.Size             = MIDI_STREAM_EPSIZE,
						.Banks            = 1,
					},
				.DataOUTEndpoint          =
					{
						.Address          = MIDI_STREAM_OUT_EPADDR,
						.Size             = MIDI_STREAM_EPSIZE,
						.Banks            = 1,
					},
			},
	};

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  MIDI_Device_ConfigureEndpoints(&MIDI_Interface);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  MIDI_Device_ProcessControlRequest(&MIDI_Interface);
}

/* end LUFA MIDI Class driver specific definitions*/

#include "util.h"
#include "keypad.h"
#include "eventqueue.h"
#include "gpio.h"
#include "analog.h"

#define MIDI_CABLENUMBER 1

static void process_incoming_midi(void)
{
  MIDI_EventPacket_t ReceivedMIDIEvent;
  while(MIDI_Device_ReceiveEventPacket(&MIDI_Interface, &ReceivedMIDIEvent)) {
    uint8_t cmd = ReceivedMIDIEvent.Data1 & 0xF0;
    uint8_t note = ReceivedMIDIEvent.Data2;
    uint8_t value = ReceivedMIDIEvent.Data3 & 0x7F;
    if(cmd == MIDI_COMMAND_PROGRAM_CHANGE) {
      switch(note) {
        case 127: reset2bootloader();
      }
      continue;
    }
    if((note >= KEYPAD_MIDI_NOTE_OFFSET && note < KEYPAD_MIDI_NOTE_OFFSET + KEYPAD_NUM_KEYS) ||     // KEYPAD
       note == KEYPAD_MIDI_NOTE_ALL) {
      if(cmd == MIDI_COMMAND_NOTE_ON) {
        switch(value) {
          case 0: keypad_led_on(note - KEYPAD_MIDI_NOTE_OFFSET); break;
          case 1: keypad_led_toggle(note - KEYPAD_MIDI_NOTE_OFFSET); break;
          default: keypad_led_blink(note, value); break;
        }
      } else if(cmd == MIDI_COMMAND_NOTE_OFF)
        keypad_led_off(note - KEYPAD_MIDI_NOTE_OFFSET);

    } else if((note >= GPIO_MIDI_NOTE_OFFSET && note < GPIO_MIDI_NOTE_OFFSET + GPIO_NUM_OUTPUTS) ||   // GPIO
              note == GPIO_MIDI_NOTE_ALL_OUTPUTS) {
      if(cmd == MIDI_COMMAND_NOTE_ON) {
        switch(value) {
          case 0: gpio_out_on(note - GPIO_MIDI_NOTE_OFFSET); break;
          case 1: gpio_out_toggle(note - GPIO_MIDI_NOTE_OFFSET); break;
        }
      } else if(cmd == MIDI_COMMAND_NOTE_OFF)
        gpio_out_off(note - GPIO_MIDI_NOTE_OFFSET);

    } else if((note >= ANALOG_MIDI_NOTE_OFFSET && note < ANALOG_MIDI_NOTE_OFFSET + ANALOG_NUM_INPUTS) ||   // ANALOG
              note == ANALOG_MIDI_NOTE_ALL_INPUTS) {
      switch(cmd) {
        case MIDI_COMMAND_NOTE_OFF: analog_disable_channel(note - ANALOG_MIDI_NOTE_OFFSET); break;
        case MIDI_COMMAND_NOTE_ON: analog_enable_channel(note - ANALOG_MIDI_NOTE_OFFSET); break;
      }
    }
  }
}

static void process_outgoing_midi(void)
{
  MIDI_EventPacket_t MIDIEventOn = (MIDI_EventPacket_t)
    {
      .Event       = MIDI_EVENT(MIDI_CHANNEL(MIDI_CABLENUMBER), MIDI_COMMAND_NOTE_ON),
      .Data1       = MIDI_COMMAND_NOTE_ON | 0,
      .Data2       = 0,
      .Data3       = 0x7F,
    };
  MIDI_EventPacket_t MIDIEventOff = (MIDI_EventPacket_t)
    {
      .Event       = MIDI_EVENT(MIDI_CHANNEL(MIDI_CABLENUMBER), MIDI_COMMAND_NOTE_OFF),
      .Data1       = MIDI_COMMAND_NOTE_OFF | 0,
      .Data2       = 0,
      .Data3       = 0,
    };
  MIDI_EventPacket_t MIDIEventAnalog = (MIDI_EventPacket_t)
    {
      .Event       = MIDI_EVENT(MIDI_CHANNEL(MIDI_CABLENUMBER), MIDI_COMMAND_CONTROL_CHANGE),
      .Data1       = MIDI_COMMAND_CONTROL_CHANGE | 0,
      .Data2       = 0,
      .Data3       = 0,
    };

  uint8_t key, state;
  while(eventqueue_pop(&key, &state)) {
    if (USB_DeviceState == DEVICE_STATE_Configured) {
      MIDI_EventPacket_t* MIDIEvent = NULL;
      if(key >= ANALOG_MIDI_NOTE_OFFSET && key < ANALOG_MIDI_NOTE_OFFSET + ANALOG_NUM_INPUTS) {
        MIDIEvent = &MIDIEventAnalog;
        MIDIEvent->Data2 = key;
        MIDIEvent->Data3 = analog_get_value(key - ANALOG_MIDI_NOTE_OFFSET);
      } else {
        MIDIEvent = state ? &MIDIEventOn : &MIDIEventOff;
        MIDIEvent->Data2 = key;
      }

      if(MIDIEvent)
        MIDI_Device_SendEventPacket(&MIDI_Interface, MIDIEvent);
    }
  }
  if (USB_DeviceState == DEVICE_STATE_Configured)
    MIDI_Device_Flush(&MIDI_Interface);
}

int main(void)
{
  MCUSR &= ~(1 << WDRF);
  wdt_disable();

  cpu_init();
  eventqueue_init();
  keypad_init();
  gpio_init();
  analog_init();

  USB_Init();
  sei();

  for(;;) {
    process_incoming_midi();

    keypad_task();
    gpio_task();
    analog_task();

    process_outgoing_midi();

    MIDI_Device_USBTask(&MIDI_Interface);
    USB_USBTask();
  }
}
