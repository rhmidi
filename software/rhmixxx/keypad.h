/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RHMIXXX_keypad_h_INCLUDED
#define RHMIXXX_keypad_h_INCLUDED

#define KEYPAD_NUM_COLS 4
#define KEYPAD_NUM_ROWS 4
#define KEYPAD_NUM_KEYS KEYPAD_NUM_COLS * KEYPAD_NUM_ROWS
#define KEYPAD_MIDI_NOTE_OFFSET 0
#define KEYPAD_MIDI_NOTE_ALL KEYPAD_MIDI_NOTE_OFFSET + 31

void keypad_init(void);

void keypad_led_on(uint8_t led);
void keypad_led_off(uint8_t led);
void keypad_led_toggle(uint8_t led);
void keypad_led_blink(uint8_t led, uint8_t value);

void keypad_task(void);

#endif
