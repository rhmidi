/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <util/delay.h>

#include "keypad.h"
#include "eventqueue.h"

#define KEYPAD_PIN PINA
#define KEYPAD_PORT PORTA
#define KEYPAD_DDR DDRA

#define KEYPAD_LP_CNT_MAX 200
static struct {
  uint8_t last_sent;
  int16_t lp_cnt;
} keypad_state[KEYPAD_NUM_KEYS];

static struct {
  uint16_t timebase;
  uint16_t cnt;
} led_blink_state[KEYPAD_NUM_KEYS];
#define KEYPAD_BLINK_TIMEBASE_MULT 64

void keypad_init(void)
{
  KEYPAD_DDR = 0x00;
  KEYPAD_PORT = 0x0F;
  uint8_t i;
  for(i = 0; i < KEYPAD_NUM_KEYS; ++i) {
    keypad_state[i].last_sent = 0;
    keypad_state[i].lp_cnt = 0;

    led_blink_state[i].timebase = 0;
    led_blink_state[i].cnt = 0;
  }

  DDRC = 0xFF;
  DDRB |= 0xF0;
  DDRD |= 0xF0;

  PORTC = 0x00;
  PORTB &= 0x0F;
  PORTD &= 0x0F;
}

void keypad_led_on(uint8_t led)
{
  switch(led) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7: PORTC |= (1 << led); break;
    case 8:
    case 9:
    case 10:
    case 11: PORTB |= (1 << (led - 8 + 4)); break;
    case 12:
    case 13:
    case 14:
    case 15: PORTD |= (1 << (led - 12 + 4)); break;
    case KEYPAD_MIDI_NOTE_ALL - KEYPAD_MIDI_NOTE_OFFSET: PORTC = 0xFF; PORTB |= 0xF0; PORTD |= 0xF0; break;
  }
  keypad_led_blink(led, 0);
}

void keypad_led_off(uint8_t led)
{
  switch(led) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7: PORTC &= ~(1 << led); break;
    case 8:
    case 9:
    case 10:
    case 11: PORTB &= ~(1 << (led - 8 + 4)); break;
    case 12:
    case 13:
    case 14:
    case 15: PORTD &= ~(1 << (led - 12 + 4)); break;
    case KEYPAD_MIDI_NOTE_ALL - KEYPAD_MIDI_NOTE_OFFSET: PORTC = 0x00; PORTB &= 0x0F; PORTD &= 0x0F; break;
  }
  keypad_led_blink(led, 0);
}

void keypad_led_toggle(uint8_t led)
{
  switch(led) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7: PORTC ^= (1 << led); break;
    case 8:
    case 9:
    case 10:
    case 11: PORTB ^= (1 << (led - 8 + 4)); break;
    case 12:
    case 13:
    case 14:
    case 15: PORTD ^= (1 << (led - 12 + 4)); break;
    case KEYPAD_MIDI_NOTE_ALL - KEYPAD_MIDI_NOTE_OFFSET: PORTC ^= 0xFF; PORTB ^= 0xF0; PORTD ^= 0xF0; break;
  }
}

static uint8_t keypad_led_get_state(uint8_t led)
{
  switch(led) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7: return (PORTC & (1 << led)) ? 1 : 0;
    case 8:
    case 9:
    case 10:
    case 11: return (PORTB & (1 << (led - 8 + 4))) ? 1 : 0;
    case 12:
    case 13:
    case 14:
    case 15: return (PORTD & (1 << (led - 12 + 4))) ? 1 : 0;
    default: return 0;
  }
}

static void keypad_led_blink_sync(uint8_t led)
{
  uint8_t i;
  for(i = 0; i < KEYPAD_NUM_KEYS; ++i) {
    if(i != led && led_blink_state[i].timebase == led_blink_state[led].timebase) {
      led_blink_state[led].cnt = led_blink_state[i].cnt;
      if(keypad_led_get_state(led) != keypad_led_get_state(i))
        keypad_led_toggle(led);
      break;
    }
  }
}

void keypad_led_blink(uint8_t led, uint8_t value)
{
  if(led < KEYPAD_NUM_KEYS) {
    led_blink_state[led].timebase = value * KEYPAD_BLINK_TIMEBASE_MULT;
    led_blink_state[led].cnt = 0;
    if(value)
      keypad_led_blink_sync(led);

  } else {
    uint8_t i;
    for(i = 0; i < KEYPAD_NUM_KEYS; ++i) {
      led_blink_state[i].timebase = value * KEYPAD_BLINK_TIMEBASE_MULT;
      led_blink_state[i].cnt = 0;
    }
  }
}

static inline void keypad_key_lowpass(uint8_t key_idx, uint8_t current_state)
{
  keypad_state[key_idx].lp_cnt += current_state ? -1 : +1;
  if(keypad_state[key_idx].lp_cnt <= 0 ||
     keypad_state[key_idx].lp_cnt >= KEYPAD_LP_CNT_MAX) {

    keypad_state[key_idx].lp_cnt = keypad_state[key_idx].lp_cnt <= 0 ? 0 : KEYPAD_LP_CNT_MAX;

    if(current_state != keypad_state[key_idx].last_sent) {
      keypad_state[key_idx].last_sent = current_state;
      eventqueue_push(KEYPAD_MIDI_NOTE_OFFSET + key_idx, ((current_state) ? 0 : 1));
    }
  }
}

static inline void keypad_led_blinking(uint8_t key_idx)
{
  if(led_blink_state[key_idx].timebase > 1) {
    if(++led_blink_state[key_idx].cnt >= led_blink_state[key_idx].timebase) {
      keypad_led_toggle(key_idx);
      led_blink_state[key_idx].cnt = 0;
    }
  }
}

void keypad_task(void)
{
  uint8_t col, row;
  for(col = 0; col < KEYPAD_NUM_COLS; ++col) {
    KEYPAD_DDR = 1 << (col + 4);
    KEYPAD_PORT = 0x0F;
    _delay_us(10);

    for(row = 0; row < KEYPAD_NUM_ROWS; ++row) {
      uint8_t key_idx = col*KEYPAD_NUM_ROWS + row;

      uint8_t current_state = KEYPAD_PIN & (1 << row);
      keypad_key_lowpass(key_idx, current_state);

      keypad_led_blinking(key_idx);
    }
  }
  KEYPAD_DDR = 0x00;
}
