/*
 *  rhmidi
 *
 *  Copyright (C) 2014-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhmidi.
 *
 *  rhmidi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhmidi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhmidi. If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include "gpio.h"
#include "eventqueue.h"

#define GPIO_PIN PINE
#define GPIO_PORT PORTE
#define GPIO_DDR DDRE

#define GPIO_INPUT_LP_CNT_MAX 200
static struct {
  uint8_t last_sent;
  int16_t lp_cnt;
} gpio_input_state[GPIO_NUM_INPUTS];


void gpio_init(void)
{
  GPIO_DDR = 0x0F;
  GPIO_PORT = 0xF0;

  uint8_t i;
  for(i = 0; i < GPIO_NUM_INPUTS; ++i) {
    gpio_input_state[i].last_sent = 0;
    gpio_input_state[i].lp_cnt = 0;
  }
}

void gpio_out_on(uint8_t num)
{
  if(num < GPIO_NUM_OUTPUTS)
    GPIO_PORT |= (1 << num);
  else
    GPIO_PORT |= 0x0F;
}

void gpio_out_off(uint8_t num)
{
  if(num < GPIO_NUM_OUTPUTS)
    GPIO_PORT &= ~(1 << num);
  else
    GPIO_PORT &= 0xF0;
}

void gpio_out_toggle(uint8_t num)
{
  if(num < GPIO_NUM_OUTPUTS)
    GPIO_PORT ^= (1 << num);
  else
    GPIO_PORT ^= 0x0F;
}

void gpio_task(void)
{
  uint8_t i;
  for(i = 0; i < GPIO_NUM_INPUTS; ++i) {
    uint8_t current_state = GPIO_PIN & (1<<(i+4));

    gpio_input_state[i].lp_cnt += current_state ? -1 : +1;
    if(gpio_input_state[i].lp_cnt <= 0 ||
       gpio_input_state[i].lp_cnt >= GPIO_INPUT_LP_CNT_MAX) {

      gpio_input_state[i].lp_cnt = gpio_input_state[i].lp_cnt <= 0 ? 0 : GPIO_INPUT_LP_CNT_MAX;
      if(current_state != gpio_input_state[i].last_sent) {
        gpio_input_state[i].last_sent = current_state;
        eventqueue_push(GPIO_MIDI_NOTE_OFFSET + i, ((current_state) ? 0 : 1));
      }
    }
  }
}
